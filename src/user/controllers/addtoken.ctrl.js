(function(angular) {
    'use strict';

    angular.module('gitlabKBApp.user').controller('TokenController', 
        [
            '$scope', 
            '$http', 
            '$state', 
            function ($scope, $http, $state) {
                $scope.isSaving = false;

                $scope.options  = [
                    {id: "gitlab", label: "GitLab"}                    
                ];
                $scope.token = {
                    provider: $scope.options[0].id
                };

                $scope.submit = function () {
                    $scope.isSaving = true;
                    $http.post('/api/save_token', {_token: $scope.token.token}).then(function (result) {
                        $state.go('board.boards');
                    });
                }
            }
        ]
    );
})(window.angular);
