(function (angular) {
    'use strict';
    angular.module('gitlabKBApp.board').factory('LabelService',
        [
            '$q',
            '$http',
            function ($q, $http) {
                return {
                    labels: [],
                    list: function (boardId) {
                        return $q.when(_.isEmpty(this.labels[boardId]) ? $http.get('/api/labels', {params: {board_id: boardId}}).then(function (result) {
                            var labels = result.data.data;
                            if (_.isEmpty(labels)) {
                                return {};
                            }

                            var pattern = /KB\[stage\]\[\d\]\[(.*)\]/;
                            var reserved = _.sortBy(_.filter(labels, function(label) {
                                return pattern.test(label.name);
                            }), "name");

                            if (_.isEmpty(reserved)) {
                                return {};
                            }

                            this.labels[boardId] = labels;
                            return this.labels[boardId];
                        }.bind(this))
                            : this.labels[boardId]);
                    }
                };
            }
        ]
    );
})(window.angular);
