(function (angular) {
    'use strict';

    angular.module('gitlabKBApp.board').controller(
        'ConfigurationController',
        [
            '$scope',
            '$http',
            '$state',
            '$stateParams',
            'BoardService',
            function ($scope, $http, $state, $stateParams, BoardService) {
                $scope.isSaving = false;

                $scope.configure = function () {
                    $scope.isSaving = true;
                    $http.post(
                        '/api/boards/configure',
                        {
                            project_id: $stateParams.project_id
                        }
                    ).then(function (result) {
                        BoardService.get($stateParams.project_id).then(function(result) {
                            $scope.isSaving = false;
                            $state.go('board.cards', {project_id: $stateParams.project_id});
                        });
                    });
                };
            }
        ]
    );
})(window.angular);
