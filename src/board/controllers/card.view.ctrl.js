(function(angular) {
    'use strict';

    angular.module('gitlabKBApp.board').controller('ViewController',
        [
            '$scope',
            '$http',
            '$stateParams',
            '$state',
            'BoardService',
            '$sce',
            'CommentService',
            'LabelService',
            'UserService',
            'MilestoneService',
            '$modal',
            function ($scope, $http, $stateParams, $state, BoardService, $sce, CommentService, LabelService, UserService, MilestoneService, $modal) {

                $scope.commentFormData = {};
                $scope.model = {};
                $scope.todoFormData = {};
                $scope.submitComment = function () {
                    $scope.isSaving = true;

                    CommentService.create($stateParams.project_id, $stateParams.issue_id, $scope.commentFormData.comment).then(function (data) {
                        $scope.isSaving = false;
                        $scope.commentFormData = {};
                        $scope.comments.push(data);
                    });
                };

                $scope.submitTodo = function (card) {
                    $scope.isSavingTodo = true;

                    card.todo.push({
                        'checked': false,
                        'body': $scope.todoFormData.body
                    });
                    BoardService.updateCard(card).then(function () {
                        $scope.isSavingTodo = false;
                        $scope.todoFormData = {};
                        $scope.isTodoAdd = true;
                    });
                };

                $scope.remove = function (card) {
                    BoardService.removeCard($stateParams.project_id, card).then(function (result) {
                        $modal.close();
                    });
                };

                $scope.updateTodo = function (card) {
                    $scope.isSavingTodo = true;
                    return BoardService.updateCard(card).then(function(){
                        $scope.isSavingTodo = false;
                    });
                };

                $scope.removeTodo = function (index, card) {
                    $scope.isSavingTodo = true;
                    card.todo.splice(index, 1);
                    return BoardService.updateCard(card).then(function() {
                        $scope.isSavingTodo = false;
                    });
                };

                $scope.update = function (card, user) {
                    if (!card.assignee || (card.assignee.id != user.id)) {
                        card.assignee_id = user.id;
                        return BoardService.updateCard(card);
                    }
                };

                $scope.updateMilestone = function (card, milestone) {
                    if (!card.milestone || (card.milestone.id != milestone.id)) {
                        card.milestone_id = milestone.id;
                        return BoardService.updateCard(card);
                    }
                };

                $scope.updateLabels = function(card, label) {
                    BoardService.get($stateParams.project_id).then(function(board) {
                        if (card.labels.length === card.viewLabels.length) {
                            card.labels.push(board.stages[0].label);
                        }

                        if (card.labels.indexOf(label.name) !== -1) {
                            card.viewLabels.splice(card.viewLabels.indexOf(label), 1);
                            card.labels.splice(card.labels.indexOf(label.name), 1);
                        } else {
                            card.viewLabels.push(label);
                            card.labels.push(label.name);
                        }

                        return BoardService.updateCard(card);
                    });
                };

                BoardService.get($stateParams.project_id).then(function(board) {
                    $scope.labels = _.toArray(board.viewLabels);
                });

                BoardService.getCard($stateParams.project_id, $stateParams.issue_id).then(function (result) {
                    $scope.card = result;
                });

                CommentService.list($stateParams.project_id, $stateParams.issue_id).then(function (data) {
                    $scope.comments = data;
                });

                MilestoneService.list($stateParams.project_id).then(function(milestones) {
                    $scope.milestones = milestones;
                });

                UserService.list($stateParams.project_id).then(function(users) {
                    $scope.options = users;
                });
            }
        ]
    );
})(window.angular);
