(function(angular) {
    'use strict';
    angular.module('gitlabKBApp.board').controller('NewIssueController', 
        [
            '$scope', 
            '$http', 
            '$stateParams', 
            '$location', 
            'BoardService', 
            'users',
            'milestones',
            'LabelService',
            '$modal',
            function ($scope, $http, $stateParams, $location, BoardService, users, milestones, LabelService, $modal) {
                $scope.options = users;
                $scope.milestones = milestones;

                $scope.isSaving = false;

                $scope.card = {
                    project_id: $stateParams.project_id,
                    labels: []
                };

                $scope.update = function(user) {
                    $scope.card.assignee = user;
                };

                $scope.updateMilestone = function(milestone) {
                    $scope.card.milestone = milestone;
                };

                $scope.updateLabels = function(label) {
                    if ($scope.card.labels.indexOf(label) !== -1) {
                        $scope.card.labels.splice($scope.card.labels.indexOf(label), 1);
                    } else {
                        $scope.card.labels.push(label);
                    }
                };

                LabelService.list($stateParams.project_id).then(function(result) {
                    BoardService.get($stateParams.project_id).then(function(board) {
                        var labels = _.filter(result, function(label) {
                            return board.labels.indexOf(label.name) === -1;
                        });

                        $scope.labels = labels;
                    });
                });


                $scope.createIssue = function () {
                    $scope.isSaving = true;

                    var data = {
                        project_id: $scope.card.project_id,
                        title: $scope.card.title,
                        description: $scope.card.description,
                    };

                    if (!_.isEmpty($scope.card.assignee)) {
                        data.assignee_id = $scope.card.assignee.id;
                    }
                    if (!_.isEmpty($scope.card.milestone)) {
                        data.milestone_id = $scope.card.milestone.id;
                    }

                    BoardService.get(data.project_id).then(function(board) {
                        var stage = _.first(board.stages);
                        var labels = [stage.label];

                        if (!_.isEmpty($scope.card.labels)) {
                            for (var i = 0; i < $scope.card.labels.length; i++) {
                                labels.push($scope.card.labels[i].name);
                            }
                            
                        }
                        data.labels = labels.join(', ');

                        BoardService.createCard(data).then(function () {
                            $modal.close();
                        });
                    });
                    
                };
            }
        ]
    );
})(window.angular);

