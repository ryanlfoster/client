#!/bin/sh
set -e

sed -i "s|GITLAB_HOST|$GITLAB_HOST|g" /var/www/client/web/assets/js/app.min.js

exec "$@"
